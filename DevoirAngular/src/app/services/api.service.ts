import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { observable, Observable } from 'rxjs';

import { User } from '../objectBox/user';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  users : User[];

  constructor(private httpClient:HttpClient) {
    this.users = [];
  }

  private callApi(limit = 10): Observable<any>
  {
    return this.httpClient.get("https://randomuser.me/api?results="+limit);
  }

  public addUsers(limit = 10): Observable<any>{
    return new Observable(observer => {
      return this.callApi(limit).subscribe(
        data=>{
          let pair = false;
          for(let singleUser of data.results){
            let user = new User();
            user.genre = singleUser.gender;
            user.name = singleUser.name['first'];
            user.country = singleUser.location.country;
            user.photo = singleUser.picture["large"];
            user.pair = pair;
            this.users.push(user);
            pair = !pair;
          }
          observer.next(this.users);
          observer.complete();
        },
        error=>{
          console.log("error");
          //alert("API Problem");
        }
      );
    });
  }

  public test(limit = 10): Observable<any>{
    return new Observable(observer => {
      for(let i = 0 ; i< 10 ; i++){
        observer.next(i);
      }
      observer.complete();
    });
  }
}
