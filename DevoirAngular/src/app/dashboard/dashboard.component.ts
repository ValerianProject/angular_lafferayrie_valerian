import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../objectBox/user';

import { ApiService } from '../services/api.service';

import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  userSelected : User;
  users : User[]; 
  indexOflistIPair : number;

  genreSelected : string;
  nameSelected : string;
  countrySelected : string;

  inputDeep : number;

  constructor(private apiService: ApiService) {
    this.userSelected = null;
    this.users= null;
    this.genreSelected = "";
    this.nameSelected = "";
    this.countrySelected = "";
    this.inputDeep = 0;
    this.indexOflistIPair = -1;
  }

  ngOnInit(): void {
    this.getUser();
  }

  getUser(number = 20){
    this.apiService.addUsers(number).subscribe({
      next:(users)=>{
        this.users = users;
        //console.log(this.users);
      },
      complete:()=>{
        this.userSelected = this.users[0];
        console.log("Le User selectionné est :")
        console.log(this.userSelected);
      }
    });
  }

  getGenre(){
    return this.userSelected.genre;
  }

  getName(){
    return this.userSelected.name;
  }

  getCountry(){
    return this.userSelected.country;
  }

  getPhoto(){
    return this.userSelected.photo;
  }

  getVisibility(){
    return 'visible'; //visible hidden
  }

  onSubmit(form: NgForm){
    this.inputDeep++;
    this.gereInputDeep(this.inputDeep);
    switch(this.inputDeep){
      case(1):
        if(form.value["inputWhoGenre"].toUpperCase() == this.getGenre().toUpperCase())
          document.getElementById("rep1").classList.add("bg-success");
        else
          document.getElementById("rep1").classList.add("bg-danger");
        document.getElementById("reponseGenre").innerHTML = this.getGenre().toUpperCase();
        break;
      case(2):
        if(form.value["inputWhoName"].toUpperCase() == this.getName().toUpperCase())
          document.getElementById("rep2").classList.add("bg-success");
        else
          document.getElementById("rep2").classList.add("bg-danger");
        document.getElementById("reponseName").innerHTML = this.getName().toUpperCase();
        console.log(this.getName());
        break;
      case(3):
        if(form.value["inputWhoCountry"].toUpperCase() == this.getCountry().toUpperCase())
          document.getElementById("rep3").classList.add("bg-success");
        else
          document.getElementById("rep3").classList.add("bg-danger");
        document.getElementById("retry").classList.remove("d-none");
        document.getElementById("reponseCountry").innerHTML = this.getCountry().toUpperCase();
        break;
      default:
        break;
    }
  }

  gereInputDeep(x : number){
    if(x == 3){
      document.getElementById("label").innerHTML = "";
      document.getElementById("inputWhoCountry").classList.add("d-none");
      document.getElementById("submit").classList.add("d-none");
    }
    else if( x < 3 ){
      var alllabel = ["Male or Female ?","My name is ?","I live in ?"];
      var allInput = ["inputWhoGenre","inputWhoName","inputWhoCountry"];
      for(var i=0; i<allInput.length; i++){
        if(x == i){
          document.getElementById("label").innerHTML = alllabel[x];
          document.getElementById(allInput[i]).classList.remove("d-none");
        }
        else{
          document.getElementById(allInput[i]).classList.add("d-none");
        }
      }
    }
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  tryAgain(index = -1){
    if(index == -1)
      this.userSelected = this.users[this.getRandomInt(this.users.length)];
    else {
      var deleteUser = this.users.splice(index,1);
      this.userSelected = deleteUser[0];
      console.log(this.users);
      this.apiService.addUsers(2).subscribe({
        next:(users)=>{},
        complete:()=>{
          let users = this.apiService.users;
          this.users.push(users[users.length-1]);
          this.users.push(users[users.length-2]);
        }
      });
    }
    console.log("Le User selectionné est :")
    console.log(this.userSelected);
    this.genreSelected = "";
    this.nameSelected = "";
    this.countrySelected = "";
    document.getElementById("submit").classList.remove("d-none");
    document.getElementById("retry").classList.add("d-none");
    document.getElementById("reponseCountry").innerHTML ="";
    document.getElementById("reponseName").innerHTML = "";
    document.getElementById("reponseGenre").innerHTML = "";
    document.getElementById("rep1").classList.remove("bg-danger");
    document.getElementById("rep2").classList.remove("bg-danger");
    document.getElementById("rep3").classList.remove("bg-danger");
    document.getElementById("rep1").classList.remove("bg-success");
    document.getElementById("rep2").classList.remove("bg-success");
    document.getElementById("rep3").classList.remove("bg-success");
    this.inputDeep = 0;
    this.gereInputDeep(this.inputDeep);
  }

  Test(){
    this.apiService.test().subscribe({
      next:(a)=>{
        console.log(a);
      },
      complete:()=>{
        console.log("success test");
      }
    });
  }
}


